// Parse static fault trees from here: https://dftbenchmarks.utwente.nl/ffort.php
#r "nuget: FParsec"
#load "treeHelpers.fsx"

open FParsec
open TreeHelpers
open System.IO

[<RequireQualifiedAccess>]
type DftNode =
    | Toplevel of name: string
    | IntermediateEvent of name: string * gate: GateType * children: string list
    | BasicEvent of name: string * attributes: (string * string) list

module DftParser =
    let ws p = spaces >>. p .>> spaces

    let pIdentifier = pchar '"' >>. many1SatisfyL (fun ch -> ch <> '"') "identifier" .>> pchar '"'
    let pGate = 
        (skipString "or" >>. preturn Or)
        <|> (skipString "and" >>. preturn And)
        <|> (pint32 .>> (skipString "of" .>> pint32) |>> Vote)

    let pBasicAttr =
        let pLabel = many1SatisfyL (fun ch -> ch >= 'a' && ch <= 'z') "attribute name"
        let pValue = many1SatisfyL (fun ch -> ch <> ' ' && ch <> ';') "attribute value"
        pLabel .>>. (skipChar '=' >>. pValue)

    let pToplevel = pstring "toplevel" >>. spaces >>. pIdentifier |>> DftNode.Toplevel
    let pIntermediate = tuple3 pIdentifier (ws pGate) (many (ws pIdentifier)) |>> DftNode.IntermediateEvent
    let pBasic = pIdentifier .>>. (many (ws pBasicAttr)) |>> DftNode.BasicEvent

    let stmnt p = ws p .>> skipChar ';' .>> spaces

    let pStatement = stmnt (pToplevel <|> (attempt pIntermediate) <|> pBasic)
    let pDft = many pStatement

    let parse = run pDft

let dftToNodeList =
    List.choose (
        function
        | DftNode.Toplevel _ -> None
        | DftNode.IntermediateEvent (name, gate, children) -> Some (Node.IntermediateEvent (name, gate, children))
        | DftNode.BasicEvent (name, _) -> Some (Node.BasicEvent name)
    )

let convertDftFile inFile (outFile: string) =
    printfn "Converting %s" inFile
    let dft = File.ReadAllText inFile
    match DftParser.parse dft with
    | Success (dft, _, _) -> 
        let nodes = dftToNodeList dft
        let json = nodesListToJson nodes
        Directory.CreateDirectory(Path.GetDirectoryName(outFile)) |> ignore
        File.WriteAllText (outFile, json)
    | Failure (errorMsg, _, _) ->
        printfn "Error parsing %s:%s" inFile errorMsg

for file in Directory.GetFiles("./ffort") do
    let name = Path.GetFileNameWithoutExtension file
    convertDftFile file $"./trees/ffort_{name}.json"
