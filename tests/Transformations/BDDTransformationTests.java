package Transformations;

import BDD.ITE;
import FaultTree.FaultTree;
import FaultTree.BasicEvent;
import FaultTree.TreeWrapper;
import Heuristics.BreadthFirstHeuristic;
import IO.Parser;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class BDDTransformationTests {
    // Using Junit 5.3 myself

    //TODO formalize these tests. right now I'm checking manually to see if the ITE structures yield valid results

    // This tree is of the form OR(AND(A, B), C)
    @Test
    public void testSmallExampleA() {
        testBFSHeuristic("smallExampleA.json");
    }

    // This tree is of the form AND(OR(A, B), AND(C, D))
    @Test
    public void testSmallExampleB() {
        testBFSHeuristic("smallExampleB.json");
    }

    private void testBFSHeuristic(String path) {
        TreeWrapper tw = Parser.parseFile(path);
        FaultTree ft = tw.getRoot();

        BreadthFirstHeuristic heu = new BreadthFirstHeuristic();
        Map<BasicEvent, Integer> order = heu.getOrder(ft);

        ITE ite = FaultTreeToITE.createITE(ft, order);
        System.out.println(ite.toString());

        System.out.println(ite.evaluation());
    }
}
