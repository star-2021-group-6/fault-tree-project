package Transformations;

import FaultTree.FaultTree;
import FaultTree.BasicEvent;
import Heuristics.Helpers.FaultTreePaths;
import Heuristics.Heuristic;
import Main.HeuristicTest;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class PathCountTest {

    @Test
    public void testTree1() {
        printPaths(HeuristicTest.testTree1);
    }

    @Test
    public void testTree2() {
        printPaths(HeuristicTest.testTree2);
    }

    @Test
    public void testTree3() {
        printPaths(HeuristicTest.testTree3);
    }
    @Test
    public void testTree4() {
        printPaths(HeuristicTest.testTree4);
    }

    private static void printPaths(FaultTree ft) {
        Map<BasicEvent, Integer> paths = FaultTreePaths.getPaths(ft);
        for (BasicEvent be: paths.keySet()) {
            System.out.println(be.name + " " + paths.get(be));

        }
    }
}
