package FaultTree;

public abstract class AbstractFaultTreeVisitor {
    public final void visit(FaultTree tree) {
        visitAny(tree);
        if (tree instanceof IntermediateEvent)
            visitIntermediateEvent((IntermediateEvent)tree);
        else if (tree instanceof BasicEvent)
            visitBasicEvent((BasicEvent)tree);
    }

    public void visitAny(FaultTree event) {

    }

    public void visitIntermediateEvent(IntermediateEvent event) {
        visit(event.left);
        visit(event.right);
    }

    public void visitBasicEvent(BasicEvent event) {

    }
}
