package FaultTree;

import java.util.ArrayList;
import java.util.Set;

public class BasicEvent implements FaultTree {
    public int ID;
    public String name;

    private ArrayList<IntermediateEvent> parents;

    public BasicEvent(int ID, String name) {
        this.ID = ID;
        this.name = name;
        this.parents = new ArrayList<>();
    }

    /**
     * Get name of the Leaf
     *
     * @return name of the leaf
     */
    public String getName() {
        return "\"" + name + "-" + ID + "\"";
    }

    /**
     * Get ID of the Leaf
     *
     * @return ID of the Leaf
     */
    public int getID() {
        return ID;
    }

    /**
     * Get unique name for the Leaf
     *
     * @return unique name for the Leaf
     */
    public String getUniqueName() {
        return "\"" + name + "-" + ID + "\"";
    }

    /**
     * Add parent to the Leaf.
     *
     * @param parent parent of the leaf
     */
    public void addParent(IntermediateEvent parent) {
        this.parents.add(parent);
    }

    /**
     * Get parents of the Leaf
     *
     * @return parents of th eLeaf
     */
    public ArrayList<IntermediateEvent> getParents() {
        return parents;
    }

    /**
     * Create a copy of the Leaf
     *
     * @return copy of the Leaf
     */
    public FaultTree copy() {
        return new BasicEvent(ID, name);
    }

    @Override
    public Set<FaultTree> allChildren() {
        return Set.of(this);
    }

    @Override
    public Set<BasicEvent> allBasicChildren() {
        return Set.of(this);
    }

    @Override
    public int eventCount() {
        return 1;
    }

    @Override
    public int basicEventCount() {
        return 1;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return this.name + "[id=" + this.ID + ", type=basic]";

    }
}
