package FaultTree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class IntermediateEvent implements FaultTree {
    public int ID;
    public Type type;
    public String name;
    public FaultTree left;
    public FaultTree right;

    private Integer eventCount = null;
    private Integer basicEventCount = null;

    private ArrayList<IntermediateEvent> parents;

    public IntermediateEvent(int ID, String name, Type type, FaultTree left, FaultTree right) {
        this.ID = ID;
        this.name = name;
        this.type = type;

        this.left = left;
        this.left.addParent(this);

        this.right = right;
        this.right.addParent(this);

        this.parents = new ArrayList<>();
    }

    /**
     * Get a name of the IntermediateEvent
     * @return name of the IntermediateEvent
     */
    public String getName() {
        return "\"" + name + "-" + type + "\"";
    }

    /**
     * Get a unique name for the IntermediateEvent including ID
     * @return unique name of the IntermediateEvent
     */
    public String getUniqueName() {
        return "\"" + name + "-" + type + "-" + ID + "\"";
    }

    /**
     * Get the ID of the IntermediateEvent
     * @return ID of the IntermediateEvent
     */
    public int getID() {
        return ID;
    }

    /**
     * Get parents of the IntermediateEvent
     * @return list of parents of the IntermediateEvent
     */
    public ArrayList<IntermediateEvent> getParents() {
        return parents;
    }

    /**
     * Add a parent to the IntermediateEvent. Used in construction.
     * @param parent, parent to add
     */
    public void addParent(IntermediateEvent parent) {
        this.parents.add(parent);
    }

    /**
     * Creates a copy of the IntermediateEvent
     * @return copy of the IntermediateEvent
     */
    public FaultTree copy() {
        return new IntermediateEvent(ID, name, type, left.copy(), right.copy());
    }

    @Override
    public Set<FaultTree> allChildren() {
        Set<FaultTree> res = new HashSet<>();
        res.add(this);
        res.addAll(left.allChildren());
        res.addAll(right.allChildren());
        return res;
    }

    @Override
    public Set<BasicEvent> allBasicChildren() {
        Set<BasicEvent> res = new HashSet<>();
        res.addAll(left.allBasicChildren());
        res.addAll(right.allBasicChildren());
        return res;
    }

    @Override
    public int eventCount() {
        if (eventCount == null) {
            eventCount = allChildren().size();
        }
        return eventCount;
    }

    @Override
    public int basicEventCount() {
        if (basicEventCount == null) {
            basicEventCount = allBasicChildren().size();
        }
        return basicEventCount;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return this.name
                + "[ID=" + this.ID
                + ", type=" + this.type + "]"
                + "(" + left.getName() + ", "
                + right.getName() + ")";
    }
}
