package FaultTree;

import Heuristics.Heuristic;

import java.util.*;

/**
 * A wrapper class to store some handy info on a fault tree.
 */
public class TreeWrapper {
    //always exists
    private String name;
    private Map<String, FaultTree> nameMap;
    private FaultTree rootNode;

    //maybe exists
    private Heuristic heuristic = null;

    //additional representations, created when requested
    private FaultTree[] nodesList = null;
    private Map<Integer, FaultTree> intMap = null;
//--------------------- CONSTRUCTORS ---------------------
    /**
     * Create a tree wrapper given a mapping
     * @param name A name for this fault tree
     * @param treeMap a map of type String -> FaultTree
     *
     * Performs a search in the tree for the root node. If already known, please add it to the constructor.
     */
    public TreeWrapper(String name, Map<String, FaultTree> treeMap){
        this.name = name;
        this.nameMap = treeMap;
        this.rootNode = findRoot(treeMap);
    }

    /**
     * Create a tree wrapper given a mapping and a root node
     * @param name A name for this fault tree
     * @param treeMap a map of type String -> FaultTree
     * @param root the root node of the tree
     */
    public TreeWrapper(String name, Map<String, FaultTree> treeMap, FaultTree root){
        this.name = name;
        this.nameMap = treeMap;
        this.rootNode = root;
    }
    /**
     * Create a tree wrapper given a mapping
     * @param name A name for this fault tree
     * @param treeMap a map of type String -> FaultTree
     * @param heuristic A heuristic for this fault tree
     *
     * Performs a search in the tree for the root node. If already known, please add it to the constructor.
     */
    public TreeWrapper(String name, Map<String, FaultTree> treeMap, Heuristic heuristic){
        this.name = name;
        this.nameMap = treeMap;
        this.rootNode = findRoot(treeMap);
        this.heuristic = heuristic;
    }

    /**
     * Create a tree wrapper given a mapping and a root node
     * @param name A name for this fault tree
     * @param treeMap a map of type String -> FaultTree
     * @param root the root node of the tree
     * @param heuristic A heuristic for this fault tree
     */
    public TreeWrapper(String name, Map<String, FaultTree> treeMap, FaultTree root, Heuristic heuristic){
        this.name = name;
        this.nameMap = treeMap;
        this.rootNode = root;
        this.heuristic = heuristic;
    }
//--------------------- GETTERS ---------------------

    /**
     * @return The name of this fault tree
     */
    public String getName(){
        return this.name;
    }

    /**
     * @return The root node of the tree
     */
    public FaultTree getRoot(){
        return this.rootNode;
    }

    public int getSize() {
        return this.nameMap.size();
    }
    /**
     * @return the heuristic associated with this fault tree, if any.
     */
    public Heuristic getHeuristic(){
        return this.heuristic;
    }
    public String toString(){
        List<String> lines= new ArrayList<>();
        lines.add("root="+this.rootNode.getName());
        for (FaultTree ft : nameMap.values()){
            lines.add(ft.toString());
        }
        return String.join("\n", lines);
    }

    /**
     * @return an array with array[node.id] == node
     */
    public FaultTree[] asArray(){
        if (nodesList == null){
            this.makeNodesList();
        }
        return Arrays.copyOf(nodesList, nodesList.length);
    }

    /**
     * @return a map with map.get(node.id) == node
     */
    public Map<Integer, FaultTree> asIDMap(){
        if (intMap == null){
            this.makeIntMap();
        }
        return new HashMap<Integer, FaultTree>(intMap);
    }

    /**
     * @return a map with map.get(node.name) == node
     */
    public Map<String, FaultTree> asStringMap(){
        return new HashMap<String, FaultTree>(nameMap);
    }

//--------------------- SETTERS---------------------
    public void setName(String name){
        this.name = name;
    }
    public void setHeuristic(Heuristic heuristic){
        this.heuristic = heuristic;
    }
//--------------------- PRIVATE ---------------------

    /**
     * finds (a) root node of the tree. only returns first one found.
     * @param tree a map of type String -> FaultTree
     * @return Root node of a tree, or null if the tree has no root (every node has a parent).
     *
     */
    private static FaultTree findRoot(Map<String, FaultTree> tree){
        for (FaultTree ft: tree.values()){
            //faster than looping through the list, climbing the tree
            int count =0;
            while (!ft.getParents().isEmpty()) {
                ft = ft.getParents().get(0);
                count++;
                if (count > tree.values().size()){
                    //protection against mall-formed trees.
                    return null;
                }
            }
            return ft;
        }
        return null;
    }

    /**
     * Construct an array where array[node.id] -> node
     */
    private void makeNodesList(){
        this.nodesList = new FaultTree[this.nameMap.size()];
        for (FaultTree ft : this.nameMap.values()){
            this.nodesList[ft.getID()]=ft;
        }
    }

    /**
     * Constructs  a map of node.id -> node
     */
    private void makeIntMap(){
        this.intMap = new HashMap<Integer, FaultTree>(this.nameMap.size());
        for (FaultTree ft : this.nameMap.values()){
            this.intMap.put(ft.getID(), ft);
        }
    }
}
