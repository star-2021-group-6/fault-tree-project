package FaultTree;

import java.util.ArrayList;
import java.util.Set;

public interface FaultTree {
    int getID();
    String getName();
    String getUniqueName();
    void addParent(IntermediateEvent parent);
    ArrayList<IntermediateEvent> getParents();
    FaultTree copy();
    Set<FaultTree> allChildren();
    Set<BasicEvent> allBasicChildren();
    int eventCount();
    int basicEventCount();
}
