package BDD;

import java.util.Set;

/**
 * Leaf nodes in the BDD have a certain value. This corresponds to whether we had a valid path/ cut set.
 */
public class Value implements ITE {

    public boolean value;
    public Value(boolean value) {
        this.value = value;
    }

    /**
     * String notation in the ITE structure;
     *
     * @return string representation of the leaf node
     */
    public String toString() {
        return value ? "1" : "0";
    }

    /**
     * Returns the size of the tree.
     * @return size of the tree.
     */
    public int size() {
        return 1;
    }

    /**
     * Returns the amount of paths in the tree.
     * @return paths in the tree
     */
    public int paths() {
        return value ? 1 : 0;
    }
    /**
     * Tree evaluation
     * @return evaluation metrics
     */
    public String evaluation() {
        return String.format("Summary:\n size:%d\npaths:%d", size(), paths());
    }

    /**
     * Collect all ITE nodes into a set
     * @param set the set that should contain all nodes in this ITE.
     */
    public void collect(Set<ITE> set) {
        set.add(this);
    }
    /**
     * Copy of the value
     * @return copy of the value
     */
    public ITE copy() {
        return new Value(value);
    }
}
