package BDD;

import java.util.Set;

/**
 * Structures of the BDD involve nodes that are not leaf nodes. These typically have a variable X. Beneath this variable
 * X we have 2 children. By convention, the left child is for X = 1 = True, and the right child is for X = 0 = False.
 *
 * These children can either be structure again, or leaf nodes which have a boolean value. 0 for an invalid cut, and a
 * 1 for a valid cut.
 */
public class Structure implements ITE {
    public Variable variable;

    /** The 1-path where we use the ITE structure as it can be either a Leaf or a Structure */
    public ITE left;

    /** The 0-path where we use the ITE structure as it can be either a Leaf or a Structure */
    public ITE right;

    public Structure(Variable variable, ITE left, ITE right) {
        this.variable = variable;
        this.left = left;
        this.right = right;
    }

    /**
     * Return the name of the variable we are choosing on
     *
     * @return name of the variable
     */
    public String getName() {
        return variable.toString();
    }

    /** Get a string version of the ITE structure.
     *  ITE( [VARIABLE], [STRING LEFT], [STRING RIGHT] )
     *  Here left is the 1-path, and right is the 0-path.
     *
     * @return string representation of the Structure
     */
    public String toString() {
        return String.format("ITE(%s, %s, %s)", variable.toString(), left.toString(), right.toString());
    }

    /**
     * Returns the size of the tree.
     * @return size of the tree.
     */
    public int size(){
        return 1 + left.size() + right.size();
    }

    /**
     * Returns the amount of paths in the tree.
     * @return paths in the tree
     */
    public int paths() {
        return left.paths() + right.paths();
    }

    /**
     * Tree evaluation
     * @return evaluation metrics
     */
    public String evaluation() {
        return String.format("Summary:\nsize:%d\npaths:%d", size(), paths());
    }

    /**
     * Collect all ITE nodes into a set
     * @param set the set that should contain all nodes in this ITE.
     */
    public void collect(Set<ITE> set) {
        if (set.contains(this)) return;
        set.add(this);
        left.collect(set);
        right.collect(set);
    }
    /**
     * Copy the structure
     * @return copy of the structure
     */
    public ITE copy() {
        return new Structure(variable.copy(), left.copy(), right.copy());
    }
}
