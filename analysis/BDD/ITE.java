package BDD;

import java.util.Set;

public interface ITE {
    String toString();

    int size();

    int paths();

    String evaluation();

    void collect(Set<ITE> set);

    ITE copy();
}
