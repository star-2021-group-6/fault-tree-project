package BDD;

import java.sql.Struct;
import java.util.*;

public class BDDHelper {
    /**
     * test if this ITE is a tree or a DAG
     * @param root the root node of this ITE
     * @return true if the ITE is a tree, false otherwise.
     */
    public static boolean isTree(ITE root){
        if (root instanceof Value){
            return true;
        }
        Set<ITE> set = new HashSet<>();
        Deque<Structure> found = new ArrayDeque<>();
        set.add(root);
        found.add((Structure) root);

        while (!found.isEmpty()){
            //DFS, BFS, who cares
            Structure struct = found.pop();
            ITE left = struct.left;
            ITE right = struct.right;
            if (set.contains(left)||set.contains(right)) {
                //found a node twice -> DAG
                return false;
            }
            set.add(left);
            set.add(right);
            //only add them to queue if they have more nodes.
            if (left instanceof Structure) {
                found.add((Structure)left);
            }
            if (right instanceof Structure) {
                found.add((Structure)right);
            }
        }
        //Traversed the whole thing without finding anything twice, thus a tree.
        return true;
    }
    /**
     * Performs a transformation from an ITE to a DOT script.
     * @param root The root node of the ITE to be transformed.
     * @return a string containing a valid DOT graph representing the ITE.
     */
    public static String toDot(ITE root) {
        HashSet<ITE> set = new HashSet<>();
        //get all nodes in the ITE
        root.collect(set);
        List<ITE> ls = new ArrayList<>(set);
        Map<ITE, Integer> idMap = new HashMap<>();
        int nextId=0;
        // give them unique IDs
        for (ITE l : ls){
            idMap.put(l, nextId++);
        }
        //prepare nodes & edges for DOT
        List<String> nodes = new ArrayList<>(),
                     edges = new ArrayList<>();
        for (ITE l : ls){
            nodes.add(dotNode(idMap, l));
            edges.addAll(dotEdge(idMap, l));
        }
        //boilerplate to create a valid graph
        String start = "digraph G {";
        String end = "\n}";
        //concatenate all items together
        StringBuilder dot = new StringBuilder(start);
        for (String n : nodes){
            dot.append("\n\t");
            dot.append(n);
        }
        dot.append("\n\n");
        for (String e : edges){
            dot.append("\n\t");
            dot.append(e);
        }
        dot.append(end);
        return dot.toString();
    }

    /**
     * Helper function. Creates a string for a 'node' object for the given ITE node
     * @param map a mapping of ITE -> id
     * @param node the ITE node to represent
     * @return a string representation for this node.
     */
    private static String dotNode(Map<ITE, Integer> map, ITE node){
        if (node instanceof Value){
            Value val = (Value) node;
            return map.get(val)+" [label=\""+val.toString()+"\"];";
        } else {
            Structure struct = (Structure) node;
            return map.get(struct)+" [label=\""+struct.variable+"\"];";
        }
    }

    /**
     * Helper function. Creates any edges that should be created for the given ITE.
     * @param map a mapping of ITE -> id
     * @param node the ITE node to represent
     * @return a list of strings, each entry represents an edge.
     */
    private static List<String> dotEdge(Map<ITE, Integer> map, ITE node){
        if (node instanceof Value){
            return Collections.emptyList();
        } else {
            Structure struct = (Structure) node;
            List<String> edges = new ArrayList<>(2);
            edges.add(map.get(struct)+" -> "+map.get(struct.left) +" [style=\"dotted\"];");
            edges.add(map.get(struct)+" -> "+map.get(struct.right) +" [style=\"solid\"];");
            return edges;
        }
    }
}
