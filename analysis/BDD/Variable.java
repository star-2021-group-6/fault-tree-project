package BDD;

/**
 * 'Gates' or non-leaf nodes in the BDD have a variable on which the path is based.
 */
public class Variable {
    public int id;
    public String name;

    public Variable(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Return the name of the Variable.
     *
     * @return string representation of the Variable.
     */
    public String toString() {
        return name;
    }

    /**
     * Copy this variable
     */
    public Variable copy() {
        return new Variable(id, name);
    }
}
