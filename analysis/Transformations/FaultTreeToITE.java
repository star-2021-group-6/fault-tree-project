package Transformations;

import BDD.ITE;
import BDD.Structure;
import BDD.Value;
import BDD.Variable;
import FaultTree.FaultTree;
import FaultTree.BasicEvent;
import FaultTree.IntermediateEvent;
import FaultTree.Type;
import java.util.HashMap;
import java.util.Map;

public class FaultTreeToITE {


    public static ITE createITE(FaultTree faultTree, Map<BasicEvent, Integer> order) {
        return createITE2(faultTree, idOrderMapping(order));
    }

    /**
     * We create an ITE structure from the fault tree. More information inside the method.
     *
     * @param faultTree, the fault tree to convert
     * @return ITE structure
     */
    private static ITE createITE2(FaultTree faultTree, Map<Integer, Integer> order) {
        // Basic events have become the variable in the structure.
        // The left and right children have values for true and false according to the convention.
        if (faultTree instanceof BasicEvent) {
            BasicEvent be = (BasicEvent) faultTree;
            return new Structure(
                    new Variable(be.ID, be.name),
                    new Value(true),
                    new Value(false)
            );
        }

        // We know we have an intermediate event, and create the left and right ITE structures recursively.
        IntermediateEvent ie = (IntermediateEvent) faultTree;
        ITE left = createITE2(ie.left, order);
        ITE right = createITE2(ie.right, order);

        // We now apply the rules from the paper
        // We currently only have AND and OR gates.
        if (ie.type == Type.And) {
            return AND(left, right, order);
        } else {
            return OR(left, right, order);
        }
    }

    /**
     * We combine two ITE structures with an AND gate into a single ITE structure. The conversion has rules for
     * all types of transformations. If either of the ITE structures is a leaf node, we have an easy transformation.
     * However, in case we have two structures we look at their variables. With the same variables we apply different
     * rules than for different variables. The rules are then applied based on a certain ordering of the variables. The
     * formulae used have been put at the other respective methods.
     *
     * @param a, the left ITE structure
     * @param b, the right ITE structure
     * @return the left and right structure combined using AND logic.
     */
    private static ITE AND(ITE a, ITE b, Map<Integer, Integer> order) {
        if (a instanceof Structure && b instanceof Structure) {
            // Structure - Structure
            Structure a1 = (Structure) a;
            Structure b1 = (Structure) b;


            int compare = Integer.compare(order.get(a1.variable.id), order.get(b1.variable.id));
            if (compare < 0) {
                // We have that (Variable A) < (Variable B)
                return AND_XY(a1, b1, order);
            } else if (compare == 0) {
                // We have that (Variable A) = (Variable B)
                return AND_XX(a1, b1, order);
            } else {
                // We have that (Variable A) > (Variable B)
                return AND_XY(b1, a1, order);
            }
        } else if (a instanceof Structure) {
            // Structure - Value
            Structure a1 = (Structure) a;
            Value b1 = (Value) b;

            // Case b1 = true : We can return the structure a1 due to the nature of the AND-gate
            // Case b1 = false: We return false, which is the value of b1
            return b1.value ? a1 : b1;
        } else if (b instanceof Structure) {
            // Value - Structure
            Value a1 = (Value) a;
            Structure b1 = (Structure) b;

            // We have similar logic to the Structure - Value of above
            return a1.value ? b1 : a1;
        } else {
            // Value - Value
            Value a1 = (Value) a;
            Value b1 = (Value) b;

            // We implement AND-gate logic
            return new Value(a1.value && b1.value);
        }
    }

    /**
     * Requires that the structures have the same variable.
     *
     * AND equation with X = Y
     *
     * a        : ITE(x, G1, G2)
     * b        : ITE(x, H1, H2)
     * result   : ITE(x, G1 * H1, G2 * H2)
     *
     * @param a, first ITE structure
     * @param b, second ITE structure
     * @param order, ordering map for the basic events (key -> order value)
     * @return combined ITE structure
     */
    private static ITE AND_XX(Structure a, Structure b, Map<Integer, Integer> order) {
        return new Structure(
            a.variable,
            AND(a.left, b.left, order),
            AND(a.right, b.right, order)
        );
    }

    /**
     * Requires that the structures have different variables.
     * Requires that the first structure has a lower ordered variable.
     *
     * AND equation with X < Y
     *
     * a        : ITE(x, G1, G2)
     * b        : ITE(y, H1, H2)
     * result   : ITE(x, G1 * h, G2 * h)
     * h        : ITE(y, H1, H2)
     *
     * @param a, first ITE structure
     * @param b, second ITE structure
     * @param order, ordering map for the basic events (key -> order value)
     * @return combined ITE structure
     */
    private static ITE AND_XY(Structure a, Structure b, Map<Integer, Integer> order) {
        Structure h = new Structure(b.variable, b.left, b.right);
        return new Structure(
            a.variable,
            AND(a.left, h.copy(), order),
            AND(a.right, h.copy(), order)
        );
    }

    /**
     * We combine two ITE structures with an OR gate into a single ITE structure. The conversion has rules for all types
     * of transformations. If either of the ITE structures is a leaf node, we have an easy transformation. However,
     * in the case we have two structures we look at their variables. With the same variable we apply different rules
     * than for different variables. The rules are then applied based on a certain ordering of the variables. The
     * formulae used have been put at the other respective methods.
     *
     * @param a, the left ITE structure
     * @param b, the right ITE structure
     * @param order, ordering map for the basic events (key -> order value)
     * @return the left and right structure combined using OR logic.
     */
    private static ITE OR(ITE a, ITE b, Map<Integer, Integer> order) {
        if (a instanceof Structure && b instanceof Structure) {
            // Structure - Structure
            Structure a1 = (Structure) a;
            Structure b1 = (Structure) b;

            int compare = Integer.compare(order.get(a1.variable.id), order.get(b1.variable.id));
            if (compare < 0) {
                // We have that (Variable A) < (Variable B)
                return OR_XY(a1, b1, order);
            } else if (compare == 0) {
                // We have that (Variable A) = (Variable B)
                return OR_XX(a1, b1, order);
            } else {
                // We have that (Variable A) > (Variable B)
                return OR_XY(b1, a1, order);
            }
        } else if (a instanceof Structure) {
            // Structure - Value
            Structure a1 = (Structure) a;
            Value b1 = (Value) b;

            // Case b1 = true : We return true, which is the value of b1
            // Case b1 = false: We return a1 as the result will depend on the structure.
            return b1.value ? b1 : a1;
        } else if (b instanceof Structure) {
            // Value - Structure
            Value a1 = (Value) a;
            Structure b1 = (Structure) b;

            // We have similar logic to the Structure - Value of above
            return a1.value ? a1 : b1;
        } else {
            // Value - Value
            Value a1 = (Value) a;
            Value b1 = (Value) b;

            // We implement OR-gate logic
            return new Value(a1.value || b1.value);
        }
    }

    /**
     * Requires that the structures have the same variable.
     *
     * OR equation with X = Y
     *
     * a        : ITE(x, G1, G2)
     * b        : ITE(x, H1, H2)
     * result   : ITE(x, G1 + H1, G2 + H2)
     *
     * @param a, first ITE structure
     * @param b, second ITE structure
     * @param order, ordering map for the basic events (key -> order value)
     * @return combined ITE structure
     */
    private static ITE OR_XX(Structure a, Structure b, Map<Integer, Integer> order) {
        return new Structure(
            a.variable,
            OR(a.left, b.left, order),
            OR(a.right, b.right, order)
        );
    }

    /**
     * Requires that the structures have different variables.
     * Requires that the first structure has a lower ordered variable.
     *
     * OR equation with X < Y
     *
     * a        : ITE(x, G1, G2)
     * b        : ITE(x, H1, H2)
     * result   : ITE(x, G1 + h, G2 + h)
     * h        : ITE(y, H1, H2)
     *
     * @param a, first ITE structure
     * @param b, second ITE structure
     * @param order, ordering map for the basic events (key -> order value)
     * @return combined ITE structure
     */
    private static ITE OR_XY(Structure a, Structure b, Map<Integer, Integer> order) {
        Structure h = new Structure(b.variable, b.left, b.right);
        return new Structure(
            a.variable,
            OR(a.left, h.copy(), order),
            OR(a.right, h.copy(), order)
        );
    }


    /**
     * We create a mapping of ID -> Order value since we are working with names/IDs for inside the ITE structures.
     * @param order, BasicEvent order
     * @return order of ID -> value
     */
    public static Map<Integer, Integer> idOrderMapping(Map<BasicEvent, Integer> order) {
        HashMap<Integer, Integer> result = new HashMap<>();
        for (BasicEvent be: order.keySet()) {
            result.put(be.ID, order.get(be));
        }
        return result;
    }


}
