package IO;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class CSVStore {

    /**
     * converts some data to a single string in CSV format, before storing it in some file.
     * Parameters not final yet.
     *
     * @param pathname the path to the file the data should be stored in
     * @param colNames
     * @param rowNames
     * @param data
     * @throws IOException
     */
    public static void storeCSV(String pathname,
                                List<String> colNames,
                                List<String> rowNames,
                                List<List<Number>> data) throws IOException{
        //check consistency
        assert data.size()==rowNames.size() : "data/row names mismatch";
        assert data.stream().mapToInt(List::size).distinct().toArray().length == 1 : "All data rows should be of equal length";
        assert data.size()>0 : "empty data array";
        assert data.get(0).size() == colNames.size() : "data/column names mismatch";

        //create table of objects
        List<List<Object>> store = new ArrayList<>();
        for (int i=0;i<data.size();i++) {
            store.add(new ArrayList<>(data.get(i)));
            store.get(i).add(0, rowNames.get(i));
        }
        store.add(0, new ArrayList<>(colNames));
        store.get(0).add(0, "");

        //convert to single string with newlines and commas
        String payload = store.stream()
                .map(w->w.stream().map(Object::toString)
                        .collect(Collectors.joining(",")))
                .collect(Collectors.joining(System.lineSeparator()));
        //store data
        store(pathname, payload);
    }

    /**
     * Store some payload in the file specified by pathname.
     * @param pathname the path to the file. Will create directories if not present
     * @param payload the data to write to the file
     * @throws IOException It is a write operation. Of course it throws an IO exception.
     */
    public static void store(String pathname, String payload) throws IOException {
        Path path = FileSystems.getDefault().getPath(pathname);
        Files.createDirectories(path.getParent());
        Files.write(path, payload.getBytes(StandardCharsets.UTF_8));
    }

    public static void main(String[] args) throws Exception{
        Random r = new Random();
        List<List<Number>> data = new ArrayList<>();
        for (int i=0;i<10;i++){
            List<Number> curr = new ArrayList<>();
            data.add(curr);
            for (int j=0;j<10;j++){
                curr.add(r.nextInt());
            }
        }
        List<String> colNames = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");
        List<String> rowNames = new ArrayList<>(colNames);
        Collections.reverse(rowNames);
        storeCSV("csv/test.csv", colNames, rowNames, data);
    }
}
