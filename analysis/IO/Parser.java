package IO;

import FaultTree.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import java.nio;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;


public class Parser {
    public static final String DIRECTORY_NAME = "simpleTrees";

    /**
     * Function that parses a JSON file into an fault tree.
     * @param filename the name of the file. It should located at CWD / *DIRECTORY_NAME* / *filename*
     * @return A treeWrapper object containing the parsed fault tree.
     */
    public static TreeWrapper parseFile(String filename) {
        int nextID = 0;
        String rootName = null;

        JSONParser parser = new JSONParser();
        Map<String, FaultTree> tree = new HashMap<>();
        try {
            //read file
            Path path = FileSystems.getDefault().getPath(filename);
            JSONArray nodes = (JSONArray) parser.parse(new FileReader(path.toFile()));

            for (Object obj: nodes) {
                JSONObject node = (JSONObject) obj;

                String name = (String) node.get("name");
                if (node.containsKey("gate")) {
                    // gate
                    JSONObject gate = (JSONObject) node.get("gate");
                    String type = (String) gate.get("type");
                    Type gateType;

                    if      (type.equals("or")) gateType = Type.Or;
                    else if (type.equals("and")) gateType = Type.And;
                    else    throw new ParserException("Unknown gate '"+type+"' found.");

                    JSONArray causes = (JSONArray) node.get("causes");
                    if (causes.size() != 2){
                        throw new ParserException("All gates should have 2 children. Found '"+name+"' with "+causes.size());
                    }
                    String left = (String) causes.get(0);
                    String right = (String) causes.get(1);
                    if (!tree.containsKey(left)) throw new ParserException("Tree should be delivered in bottom-up fashion, found gate '"+name+"' with cause '"+left+"' which is not yet seen.");
                    if (!tree.containsKey(right)) throw new ParserException("Tree should be delivered in bottom-up fashion, found gate '"+name+"' with cause '"+right+"' which is not yet seen.");

                    FaultTree leftNode = tree.get(left);
                    FaultTree rightNode = tree.get(right);
                    tree.put(name, new IntermediateEvent(nextID++, name, gateType, leftNode, rightNode));

                } else {
                    //leaf
                    tree.put(name, new BasicEvent(nextID++, name));
                }
                //topological order, so last node seen must be root
                rootName = name;
            }
            if (rootName == null){
                throw new ParserException("Empty tree, no nodes found.");
            }
        } catch (IOException | ParseException e) {
            //ParseException is from JSONParser
            //ParserException is defined locally
            e.printStackTrace();
            throw new ParserException(e.getMessage());
        }
        return new TreeWrapper(filename, tree, tree.get(rootName));

    }

    public static void main(String[] args){
//        String[] filenames = new String[]{"safeRoadTrip.json", "ftaOverviewPaper_figure4.json"};
        Path p = FileSystems.getDefault().getPath(DIRECTORY_NAME);
        File dir = p.toFile();
        File[] files = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".json");
            }
        });
        for (File file : files) {
            String filename=file.getName();
            System.out.println("------------------------"+filename);
            TreeWrapper tw = parseFile(filename);
            if (tw.getSize() < 5) {
                System.out.println(tw.toString());
            } else {
                System.out.println("<large tree> ("+tw.getSize()+" nodes)");
            }

        }
    }

}
