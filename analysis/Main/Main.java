package Main;

import BDD.ITE;
import FaultTree.*;
import Heuristics.*;
import IO.Parser;
import Transformations.FaultTreeToITE;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws Exception {
        Stream<Path> treeFiles = Files.find(Path.of(args[0]), 5, (p, attr) -> attr.isRegularFile() && p.getFileName().toString().endsWith(".json"));
        Path csv = Path.of(args[0]).resolve("results.csv");
        Set<String> alreadyDone;
        if (Files.exists(csv)) {
            alreadyDone =
                    Files.readAllLines(csv).stream().map(line -> {
                        String[] fields = line.split(",");
                        return fields[0] + "," + fields[1];
                    }).collect(Collectors.toSet());
        } else {
            alreadyDone = new HashSet<>();
        }
        try (BufferedWriter csvWriter = new BufferedWriter(new FileWriter(csv.toFile(), true))) {
            if (alreadyDone.isEmpty()) {
                csvWriter.write("tree,heuristic,bdd_size,bdd_paths,ft_events,ft_basic_events");
                csvWriter.newLine();
            }
            treeFiles
                .map(path -> {
                    TreeWrapper tw = Parser.parseFile(path.toString());
                    FaultTree ft = tw.getRoot();
                    return new HashMap.SimpleEntry<Path, FaultTree>(path, ft);
                })
                .flatMap(e -> evaluateTree(e.getValue(), e.getKey().toString(), alreadyDone).entrySet().stream())
                .forEach(res -> {
                    try {
                        csvWriter.write(res.getKey());
                        csvWriter.write(',');
                        csvWriter.write(res.getValue());
                        csvWriter.newLine();
                        csvWriter.flush();
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                });
        }
    }

    private static Map<String, String> evaluateTree(FaultTree ft, String name, Set<String> alreadyDone) {
        System.out.println(String.format("Running on tree %s", name));

        Map<String, String> result = new HashMap<>();

        evaluateAndPutIfNotDone(name + ",DepthFirst", new DepthFirstHeuristic(), ft, result, alreadyDone);
        evaluateAndPutIfNotDone(name + ",BreadthFirst", new BreadthFirstHeuristic(), ft, result, alreadyDone);
        evaluateAndPutIfNotDone(name + ",ByLevel", new ByLevelHeuristic(), ft, result, alreadyDone);
        evaluateAndPutIfNotDone(name + ",ByWeight", new ByWeightHeuristic(), ft, result, alreadyDone);
        evaluateAndPutIfNotDone(name + ",Fanin", new FaninHeuristic(), ft, result, alreadyDone);
        evaluateAndPutIfNotDone(name + ",MostPathsFirst", new MostPathsFirstHeuristic(), ft, result, alreadyDone);
        evaluateAndPutIfNotDone(name + ",LeastPathsFirst", new LeastPathsFirstHeuristic(), ft, result, alreadyDone);
        evaluateAndPutIfNotDone(name + ",DecreasingRDFLM", new DecreasingRDFLM(), ft, result, alreadyDone);
        evaluateAndPutIfNotDone(name + ",IncreasingRDFLM", new IncreasingRDFLM(), ft, result, alreadyDone);

        return result;
    }

    private static void evaluateAndPutIfNotDone(String name, Heuristic heuristic, FaultTree ft, Map<String, String> result, Set<String> alreadyDone) {
        if (!alreadyDone.contains(name)) {
            result.put(name, evaluateTree(heuristic, ft));
        } else {
            System.out.println(String.format("- Skipping %s, already done", heuristic.getName()));
        }
    }

    private static String evaluateTree(Heuristic heuristic, FaultTree ft) {
        String ftInfo = "," + ft.eventCount() + "," + ft.basicEventCount();
        System.out.println(String.format("- Evaluating %s", heuristic.getName()));
        try {
            ITE bdd = FaultTreeToITE.createITE(ft, heuristic.getOrder(ft));
            return bdd.size() + "," + bdd.paths() + ftInfo;
        } catch (OutOfMemoryError err) {
            System.out.println("  -> Out of memory");
            return "oome,oome" + ftInfo;
        }
    }
}
