package Main;

import java.util.HashMap;

import FaultTree.*;
import Heuristics.*;

public class HeuristicTest {
    // HeuristicTest tree definitions
    static BasicEvent t1_b1 = new BasicEvent(1, "B1");
    static BasicEvent t1_b4 = new BasicEvent(4, "B4");
    static BasicEvent t1_b5 = new BasicEvent(5, "B5");
    static BasicEvent t1_b6 = new BasicEvent(6, "B6");
    public static FaultTree testTree1 =
        new IntermediateEvent(0, "Top", Type.And, 
            t1_b1,
            new IntermediateEvent(2, "E1", Type.Or,
                new IntermediateEvent(3, "E2", Type.And,
                    t1_b4,
                    t1_b5),
                t1_b6));
            
    static BasicEvent t2_b1 = new BasicEvent(2, "B1");
    static BasicEvent t2_b2 = new BasicEvent(4, "B2");
    static BasicEvent t2_b3 = new BasicEvent(5, "B3");
    static BasicEvent t2_b4 = new BasicEvent(8, "B4");
    public static FaultTree testTree2 =
        new IntermediateEvent(0, "Top", Type.And,
            new IntermediateEvent(1, "E1", Type.Or,
                t2_b1,
                new IntermediateEvent(3, "E2", Type.And,
                    t2_b2,
                    t2_b3)),
            new IntermediateEvent(6, "E3", Type.Or,
                t2_b3,
                new IntermediateEvent(7, "E4", Type.And,
                    t2_b3,
                    t2_b4)));

    static BasicEvent t3_b1 = new BasicEvent(2, "B1");
    static BasicEvent t3_b2 = new BasicEvent(4, "B2");
    static BasicEvent t3_b3 = new BasicEvent(6, "B3");
    static BasicEvent t3_b4 = new BasicEvent(8, "B4");
    static BasicEvent t3_b5 = new BasicEvent(9, "B5");
    static BasicEvent t3_b6 = new BasicEvent(13, "B6");
    static BasicEvent t3_b7 = new BasicEvent(14, "B7");
    static BasicEvent t3_b8 = new BasicEvent(17, "B8");
    static BasicEvent t3_b9 = new BasicEvent(18, "B9");
    public static FaultTree testTree3 =
        new IntermediateEvent(0, "Top", Type.And,
            new IntermediateEvent(1, "E1", Type.And,
                t3_b1,
                new IntermediateEvent(3, "E2", Type.Or,
                    t3_b2,
                    new IntermediateEvent(5, "E3", Type.And,
                        t3_b3,
                        new IntermediateEvent(7, "E4", Type.Or,
                            t3_b4,
                            t3_b5)))),
            new IntermediateEvent(10, "E5", Type.Or,
                new IntermediateEvent(11, "E6", Type.Or,
                    new IntermediateEvent(12, "E7", Type.And,
                        t3_b5,
                        t3_b6),
                    t3_b7),
                new IntermediateEvent(15, "E8", Type.And,
                    new IntermediateEvent(16, "E9", Type.And,
                        t3_b8,
                        t3_b9),
                    t3_b1)));

    static BasicEvent t4_b1 = new BasicEvent(2, "B1");
    static BasicEvent t4_b2 = new BasicEvent(4, "B2");
    static BasicEvent t4_b3 = new BasicEvent(5, "B3");
    static BasicEvent t4_b4 = new BasicEvent(8, "B4");
    static BasicEvent t4_b5 = new BasicEvent(9, "B5");
    static IntermediateEvent t4_e2 = new IntermediateEvent(3, "E2", Type.And, t4_b2, t4_b3);
    static IntermediateEvent t4_e5 = new IntermediateEvent(5, "E5", Type.And, t4_b3, t4_b4);
    static IntermediateEvent t4_e1 = new IntermediateEvent(1, "E1", Type.And, t4_b1, t4_e2);
    static IntermediateEvent t4_e4 = new IntermediateEvent(2, "E4", Type.And, t4_e2, t4_b5);
    static IntermediateEvent t4_e3 = new IntermediateEvent(4, "E3", Type.And, t4_e4, t4_e5);
    public static FaultTree testTree4 = new IntermediateEvent(0, "Top", Type.And, t4_e1, t4_e3);

    // HeuristicTest helper
    private static <T> void expectEquals(T actual, T expected, String title) {
        if (actual.equals(expected)) {
            System.out.println("✔ Passed: " + title);
        } else {
            System.out.println("❌ Failed: " + title);
            System.out.println(" Actual:   " + actual.toString());
            System.out.println(" Expected: " + expected.toString());
        }
    }

    // Tests
    private static void testByWeightHeuristic() {
        var expectedOrder1 = new HashMap<>() {{
            put(t1_b1, 3);
            put(t1_b4, 0);
            put(t1_b5, 1);
            put(t1_b6, 2);
        }};

        var expectedOrder2 = new HashMap<>() {{
            put(t2_b1, 2);
            put(t2_b2, 0);
            put(t2_b3, 1);
            put(t2_b4, 3);
        }};

        var expectedOrder3 = new HashMap<>() {{
            put(t3_b1, 5);
            put(t3_b2, 8);
            put(t3_b3, 7);
            put(t3_b4, 6);
            put(t3_b5, 0);
            put(t3_b6, 1);
            put(t3_b7, 2);
            put(t3_b8, 3);
            put(t3_b9, 4);
        }};
        
        var heuristic = new ByWeightHeuristic();
        expectEquals(heuristic.getOrder(testTree1), expectedOrder1, "By Weight Heuristic on tree 1");
        expectEquals(heuristic.getOrder(testTree2), expectedOrder2, "By Weight Heuristic on tree 2");
        expectEquals(heuristic.getOrder(testTree3), expectedOrder3, "By Weight Heuristic on tree 3");
    }

    private static void testFaninHeuristic() {
        var expectedOrder1 = new HashMap<>() {{
            put(t1_b1, 3);
            put(t1_b4, 0);
            put(t1_b5, 1);
            put(t1_b6, 2);
        }};

        var expectedOrder2 = new HashMap<>() {{
            put(t2_b1, 2);
            put(t2_b2, 0);
            put(t2_b3, 1);
            put(t2_b4, 3);
        }};

        var expectedOrder3 = new HashMap<>() {{
            put(t3_b1, 4);
            put(t3_b2, 3);
            put(t3_b3, 2);
            put(t3_b4, 0);
            put(t3_b5, 1);
            put(t3_b6, 5);
            put(t3_b7, 6);
            put(t3_b8, 7);
            put(t3_b9, 8);
        }};
        
        var heuristic = new FaninHeuristic();
        expectEquals(heuristic.getOrder(testTree1), expectedOrder1, "Fanin Heuristic on tree 1");
        expectEquals(heuristic.getOrder(testTree2), expectedOrder2, "Fanin Heuristic on tree 2");
        expectEquals(heuristic.getOrder(testTree3), expectedOrder3, "Fanin Heuristic on tree 3");
    }

    private static void testDFSHeuristic() {
        var expectedOrder1 = new HashMap<>() {{
            put(t1_b1, 0);
            put(t1_b4, 1);
            put(t1_b5, 2);
            put(t1_b6, 3);
        }};

        var expectedOrder2 = new HashMap<>() {{
            put(t2_b1, 0);
            put(t2_b2, 1);
            put(t2_b3, 2);
            put(t2_b4, 3);
        }};

        var expectedOrder3 = new HashMap<>() {{
            put(t3_b1, 0);
            put(t3_b2, 1);
            put(t3_b3, 2);
            put(t3_b4, 3);
            put(t3_b5, 4);
            put(t3_b6, 5);
            put(t3_b7, 6);
            put(t3_b8, 7);
            put(t3_b9, 8);
        }};

        var expectedOrder4 = new HashMap<>() {{
            put(t4_b1, 0);
            put(t4_b2, 1);
            put(t4_b3, 2);
            put(t4_b4, 4);
            put(t4_b5, 3);
        }};

        var heuristic = new DepthFirstHeuristic();
        expectEquals(heuristic.getOrder(testTree1), expectedOrder1, "DFS Heuristic on tree 1");
        expectEquals(heuristic.getOrder(testTree2), expectedOrder2, "DFS Heuristic on tree 2");
        expectEquals(heuristic.getOrder(testTree3), expectedOrder3, "DFS Heuristic on tree 3");
        expectEquals(heuristic.getOrder(testTree4), expectedOrder4, "DFS Heuristic on tree 4");
    }

    private static void testBFSHeuristic() {
        var expectedOrder1 = new HashMap<>() {{
            put(t1_b1, 0);
            put(t1_b4, 2);
            put(t1_b5, 3);
            put(t1_b6, 1);
        }};

        var expectedOrder2 = new HashMap<>() {{
            put(t2_b1, 0);
            put(t2_b2, 2);
            put(t2_b3, 1);
            put(t2_b4, 3);
        }};

        var expectedOrder3 = new HashMap<>() {{
            put(t3_b1, 0);
            put(t3_b2, 1);
            put(t3_b3, 3);
            put(t3_b4, 8);
            put(t3_b5, 4);
            put(t3_b6, 5);
            put(t3_b7, 2);
            put(t3_b8, 6);
            put(t3_b9, 7);
        }};

        var expectedOrder4 = new HashMap<>() {{
            put(t4_b1, 0);
            put(t4_b2, 1);
            put(t4_b3, 2);
            put(t4_b4, 4);
            put(t4_b5, 3);
        }};

        var heuristic = new BreadthFirstHeuristic();
        expectEquals(heuristic.getOrder(testTree1), expectedOrder1, "BFS Heuristic on tree 1");
        expectEquals(heuristic.getOrder(testTree2), expectedOrder2, "BFS Heuristic on tree 2");
        expectEquals(heuristic.getOrder(testTree3), expectedOrder3, "BFS Heuristic on tree 3");
        expectEquals(heuristic.getOrder(testTree4), expectedOrder4, "BFS Heuristic on tree 4");
    }

    private static void testByLevelHeuristic() {
        var expectedOrder1 = new HashMap<>() {{
            put(t1_b1, 0);
            put(t1_b4, 2);
            put(t1_b5, 3);
            put(t1_b6, 1);
        }};

        var expectedOrder2 = new HashMap<>() {{
            put(t2_b1, 0);
            put(t2_b2, 1);
            put(t2_b3, 2);
            put(t2_b4, 3);
        }};

        var expectedOrder3 = new HashMap<>() {{
            put(t3_b1, 2);
            put(t3_b2, 0);
            put(t3_b3, 3);
            put(t3_b4, 7);
            put(t3_b5, 8);
            put(t3_b6, 4);
            put(t3_b7, 1);
            put(t3_b8, 5);
            put(t3_b9, 6);
        }};

        var expectedOrder4 = new HashMap<>() {{
            put(t4_b1, 0);
            put(t4_b2, 3);
            put(t4_b3, 4);
            put(t4_b4, 2);
            put(t4_b5, 1);
        }};

        var heuristic = new ByLevelHeuristic();
        expectEquals(heuristic.getOrder(testTree1), expectedOrder1, "By level Heuristic on tree 1");
        expectEquals(heuristic.getOrder(testTree2), expectedOrder2, "By level Heuristic on tree 2");
        expectEquals(heuristic.getOrder(testTree3), expectedOrder3, "By level Heuristic on tree 3");
        expectEquals(heuristic.getOrder(testTree4), expectedOrder4, "By level Heuristic on tree 4");
    }

    private static void testEventCount() {
        expectEquals(testTree1.eventCount(), 7, "Event count on tree 1");
        expectEquals(testTree2.eventCount(), 9, "Event count on tree 2");
        expectEquals(testTree3.eventCount(), 19, "Event count on tree 3");
        expectEquals(testTree4.eventCount(), 11, "Event count on tree 4");
        expectEquals(testTree1.basicEventCount(), 4, "Basic event count on tree 1");
        expectEquals(testTree2.basicEventCount(), 4, "Basic event count on tree 2");
        expectEquals(testTree3.basicEventCount(), 9, "Basic event count on tree 3");
        expectEquals(testTree4.basicEventCount(), 5, "Basic event count on tree 4");
    }

    public static void main(String[] args) {
        testByWeightHeuristic();
        testFaninHeuristic();
        testDFSHeuristic();
        testBFSHeuristic();
        testByLevelHeuristic();
        testEventCount();
    }
}
