package Heuristics;

import FaultTree.BasicEvent;
import FaultTree.FaultTree;
import FaultTree.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class DepthFirstHeuristic extends AbstractFaultTreeVisitor implements Heuristic {

    private Map<BasicEvent, Integer> order;
    private HashSet<FaultTree> visited;
    private int counter;
    private String name = "Depth First Heuristic";

    @Override
    public Map<BasicEvent, Integer> getOrder(FaultTree tree) {
        //flush old values of order, visited and counter in case the same instance of DFS heuristic is used for multiple trees
        order = new HashMap<>();
        visited = new HashSet<>();
        counter = 0;
        if(tree == null)    { return order; }
        visit(tree);
        return this.order;
    }

    @Override
    public void visitIntermediateEvent(IntermediateEvent event) {
        if(visited.contains(event) || event == null){
            return;
        }
        visited.add(event);
        visit(event.left);
        visit(event.right);
    }

    @Override
    public void visitBasicEvent(BasicEvent event) {
        if (!order.containsKey(event)) {
            order.put(event, counter++);
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
