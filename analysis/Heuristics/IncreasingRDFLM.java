package Heuristics;

import FaultTree.*;
import Heuristics.Helpers.RepeatedEventsCounter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* order of visiting:
 -BE with higher occurrence
 -BE with smaller occurrence
  -intermediate event
 */
public class IncreasingRDFLM extends AbstractFaultTreeVisitor implements Heuristic{
    private String name = "Increasing Repeated events priority Depth-first leftmost Heuristic";
    private Map<BasicEvent, Integer> order;
    private HashSet<FaultTree> visited;
    private Map<BasicEvent, Integer> frequencyBE;
    private int counter;

    @Override
    public Map<BasicEvent, Integer> getOrder(FaultTree tree) {
        //flush old values of order, visited and counter in case the same instance of heuristic is used for multiple trees
        order = new HashMap<>();
        visited = new HashSet<>();
        frequencyBE = RepeatedEventsCounter.getFrequency(tree);
        counter = 0;
        if(tree == null)    { return order; }
        visit(tree);
        return this.order;
    }

    @Override
    public void visitIntermediateEvent(IntermediateEvent event) {
        if(visited.contains(event) || event == null){
            return;
        }
        visited.add(event);

        if(event.left instanceof BasicEvent && event.right instanceof BasicEvent){
            if(frequencyBE.get(event.left) >= frequencyBE.get(event.right)){
                visit(event.left);
                visit(event.right);
            } else{
                visit(event.right);
                visit(event.left);
            }
        }

        else if(event.left instanceof BasicEvent) {
            visit(event.left);
            visit(event.right);
        }

        else if(event.right instanceof BasicEvent){
            visit(event.right);
            visit(event.left);
        }

        else{
            visit(event.left);
            visit(event.right);
        }
    }

    @Override
    public void visitBasicEvent(BasicEvent event) {
        if (!order.containsKey(event)) {
            order.put(event, counter++);
        }
    }

    @Override
    public String getName() {
        return name;
    }

}


