package Heuristics;

import java.util.Map;
import FaultTree.*;

public interface Heuristic {
    Map<BasicEvent, Integer> getOrder(FaultTree tree);
    String getName();
}
