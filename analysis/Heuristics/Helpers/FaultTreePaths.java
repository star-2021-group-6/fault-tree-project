package Heuristics.Helpers;

import FaultTree.BasicEvent;
import FaultTree.FaultTree;
import FaultTree.IntermediateEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FaultTreePaths {
    /**
     * Find the amount of paths leading to each of the Basic Events in the Fault Tree
     * @return map of basic events with corresponding amount of paths
     */
    public static Map<BasicEvent, Integer> getPaths(FaultTree ft) {
        return countPaths(ft, new HashMap<>());
    }

    private static Map<BasicEvent, Integer> countPaths(FaultTree ft, Map<BasicEvent, Integer> counts ) {
        if (ft instanceof BasicEvent) {
            BasicEvent be = (BasicEvent) ft;
            counts.put(be, counts.getOrDefault(be, 0) + 1);
        } else {
            IntermediateEvent ie = (IntermediateEvent) ft;
            countPaths(ie.left, counts);
            countPaths(ie.right, counts);
        }
        return counts;
    }

    public static Map<BasicEvent, Integer> createOrder(ArrayList<BasicEvent> a) {
        Map<BasicEvent, Integer> result = new HashMap<>();
        for (int i = 0; i < a.size(); i++) {
            result.put(a.get(i), i);
        }
        return result;
    }
}
