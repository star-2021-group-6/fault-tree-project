package Heuristics.Helpers;

import FaultTree.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class RepeatedEventsCounter {
    /**
     * Find the amount of time each Basic Event appears in the Fault Tree
     * @return map of basic events with corresponding amount of occurence
     */
    public static Map<BasicEvent, Integer> getFrequency(FaultTree ft) {
        return countFrequency(ft, new HashMap<>());
    }

    private static Map<BasicEvent, Integer> countFrequency(FaultTree ft, Map<BasicEvent, Integer> counts ) {
        if (ft instanceof BasicEvent) {
            BasicEvent be = (BasicEvent) ft;
            counts.put(be, counts.getOrDefault(be, 0) + 1);
        } else {
            IntermediateEvent ie = (IntermediateEvent) ft;
            countFrequency(ie.left, counts);
            countFrequency(ie.right, counts);
        }
        return counts;
    }
}
