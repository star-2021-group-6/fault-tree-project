package Heuristics.Helpers;

import FaultTree.*;

import java.util.HashMap;
import java.util.Map;

public class DepthFirstByWeightOrderer extends AbstractFaultTreeVisitor {
    private Map<FaultTree, Integer> weights;
    private int counter = 0;
    private Map<BasicEvent, Integer> order = new HashMap<>();

    public DepthFirstByWeightOrderer(Map<FaultTree, Integer> weights) {
        this.weights = weights;
    }
    
    @Override
    public void visitIntermediateEvent(IntermediateEvent event) {
        if (weights.get(event.left) >= weights.get(event.right)) {
            visit(event.left);
            visit(event.right);
        } else {
            visit(event.right);
            visit(event.left);
        }
    }

    @Override
    public void visitBasicEvent(BasicEvent event) {
        if (!order.containsKey(event)) {
            order.put(event, counter++);
        }
    }
    
    public Map<BasicEvent, Integer> getOrder() {
        return order;
    }
}