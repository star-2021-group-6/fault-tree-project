package Heuristics;

import FaultTree.BasicEvent;
import FaultTree.FaultTree;
import Heuristics.Helpers.FaultTreePaths;
import FaultTree.BasicEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MostPathsFirstHeuristic implements Heuristic {
    private String name = "Most Paths First Heuristic";

    @Override
    public Map<BasicEvent, Integer> getOrder(FaultTree tree) {
        Map<BasicEvent, Integer> paths = FaultTreePaths.getPaths(tree);
        ArrayList<BasicEvent> a = new ArrayList<>(paths.keySet());
        a.sort((o1, o2) -> Integer.compare(paths.get(o2), paths.get(o1)));
        return FaultTreePaths.createOrder(a);
    }

    @Override
    public String getName() {
        return name;
    }
}
