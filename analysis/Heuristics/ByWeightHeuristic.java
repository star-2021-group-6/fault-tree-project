package Heuristics;

import java.util.HashMap;
import java.util.Map;
import FaultTree.*;
import Heuristics.Helpers.DepthFirstByWeightOrderer;

public class ByWeightHeuristic implements Heuristic {
    private String name = "By Weight Heuristic";

    private class FaultTreeScale extends AbstractFaultTreeVisitor {
        private Map<FaultTree, Integer> weights = new HashMap<>();

        @Override
        public void visitIntermediateEvent(IntermediateEvent event) {
            visit(event.left);
            visit(event.right);
            weights.put(event, weights.get(event.left) + weights.get(event.right));
        }

        @Override
        public void visitBasicEvent(BasicEvent event) {
            weights.put(event, 1);
        }

        public Map<FaultTree, Integer> getWeights() {
            return weights;
        }
    }

    @Override
    public Map<BasicEvent, Integer> getOrder(FaultTree tree) {
        var scale = new FaultTreeScale();
        scale.visit(tree);
        var weights = scale.getWeights();
        var orderer = new DepthFirstByWeightOrderer(weights);
        orderer.visit(tree);
        return orderer.getOrder();
    }

    @Override
    public String getName() {
        return name;
    }
}