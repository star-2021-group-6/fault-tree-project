package Heuristics;

import FaultTree.BasicEvent;
import FaultTree.FaultTree;
import Heuristics.Helpers.FaultTreePaths;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class LeastPathsFirstHeuristic implements Heuristic {
    private String name = "Least Paths First Heuristic";

    @Override
    public Map<BasicEvent, Integer> getOrder(FaultTree tree) {
        Map<BasicEvent, Integer> paths = FaultTreePaths.getPaths(tree);
        ArrayList<BasicEvent> a = new ArrayList<>(paths.keySet());
        a.sort(Comparator.comparingInt(paths::get));
        return FaultTreePaths.createOrder(a);
    }

    @Override
    public String getName() {
        return name;
    }

}
