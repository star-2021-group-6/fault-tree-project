package Heuristics;

import FaultTree.BasicEvent;
import FaultTree.FaultTree;
import FaultTree.IntermediateEvent;

import java.util.*;

public class BreadthFirstHeuristic implements Heuristic{

    Map<BasicEvent, Integer> order;
    HashSet<FaultTree> seen;
    private String name = "Breadth First Heuristic";

    @Override
    public Map<BasicEvent, Integer> getOrder(FaultTree tree) {
        order = new HashMap<>();
        seen = new HashSet<>();
        int counter = 0;

        LinkedList<FaultTree> list = new LinkedList<>();
        list.add(tree);

        while(list.peek() != null){
            LinkedList<FaultTree> temp = new LinkedList<>();
            for (FaultTree event : list) {
                if (seen.contains(event)) { continue; }
                else if (event instanceof BasicEvent) {
                    order.put((BasicEvent) event, counter++);
                } else {
                    temp.add(((IntermediateEvent) event).left);
                    temp.add(((IntermediateEvent) event).right);
                }
                seen.add(event);
            }

            list = temp;

        }
        return order;
    }

    @Override
    public String getName() {
        return name;
    }
}
