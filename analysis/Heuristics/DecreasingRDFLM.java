package Heuristics;

import FaultTree.*;
import Heuristics.Helpers.RepeatedEventsCounter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* order of visiting:
 -intermediate event
 -BE with smaller occurrence
 -BE with higher occurrence
 */
public class DecreasingRDFLM extends AbstractFaultTreeVisitor implements Heuristic{
    private String name = "Decreasing Repeated events priority Depth-first leftmost Heuristic";
    private Map<BasicEvent, Integer> order;
    private HashSet<FaultTree> visited;
    private Map<BasicEvent, Integer> frequencyBE;
    private int counter;

    @Override
    public Map<BasicEvent, Integer> getOrder(FaultTree tree) {
        //flush old values of order, visited and counter in case the same instance of DFS heuristic is used for multiple trees
        order = new HashMap<>();
        visited = new HashSet<>();
        frequencyBE = RepeatedEventsCounter.getFrequency(tree);
        counter = 0;
        if(tree == null)    { return order; }
        visit(tree);
        return this.order;
    }

    @Override
    public void visitIntermediateEvent(IntermediateEvent event) {
        if(visited.contains(event) || event == null){
            return;
        }
        visited.add(event);
        if(event.left instanceof IntermediateEvent){
            visit(event.left);
            visit(event.right);
        } else if(event.right instanceof IntermediateEvent) {
            visit(event.right);
            visit(event.left);
        } else{
            int leftOccurrence = event.left instanceof BasicEvent ? frequencyBE.get(event.left) : 0;
            int rightOccurrence = event.right instanceof BasicEvent ? frequencyBE.get(event.right) : 0;
            if(leftOccurrence <= rightOccurrence){
                visit(event.left);
                visit(event.right);
            } else{
                visit(event.right);
                visit(event.left);
            }
        }
    }

    @Override
    public void visitBasicEvent(BasicEvent event) {
        if (!order.containsKey(event)) {
            order.put(event, counter++);
        }
    }


    @Override
    public String getName() {
        return name;
    }



}
