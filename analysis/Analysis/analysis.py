'''
Currently only "oome" recognized. Add more in errors array.

Overall Analysis:
	- Plots scatter of each tree for different heuristics
	- Removes trees that resulted in same bdd_size for each tree
	- Errors are replaced by errorValue (1e8)

Bucket Analyis:
	- Create graph with buckets between factor 1 and 5
	- Seperate bucket at 0.9 which includes trees that gave an error or had a factor > 5
	- Tree is skipped if all heuristics had an error

Bar Analyis:
	- For each tree gives the amount of bdd_size for each heuristic
	- Errors are replaced by errorValue (1e8)
'''

from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import csv
import sys

if len(sys.argv) <= 1:
	print('Please provide a file name in the command line')
	print('Example:\npython analysis.py test.csv (results_folder)')
	exit()

fileName = sys.argv[1]
folder = sys.argv[2] if len(sys.argv) >= 3 else './'
extension = ".png"

df = pd.read_csv(fileName)

# Get tree and heuristic names
trees = df['tree'].unique().tolist()

#heuristics = df['heuristic'].unique().tolist()
# hack since df is not uniquely ordered between csvs
heuristics = ["BreadthFirst", "DepthFirst", "ByLevel", "MostPathsFirst", "LeastPathsFirst", "Fanin", "ByWeight", "IncreasingRDFLM", "DecreasingRDFLM"]

# Save an image
def saveImage(fileName, legend=True, xlabel="", ylabel="", yscale="linear"):
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.yscale(yscale)
	if legend: plt.legend()
	plt.savefig(folder + fileName + extension, bbox_inches="tight")
	plt.clf()

# Removes error values from the lists
errors = ["oome"]
def filterErrors(a, b):
	assert(len(a) == len(b))
	x, y = [], []
	for i in range(len(a)):
		if a[i] not in  errors and b[i] not in errors: 
			x.append(a[i])
			y.append(b[i])
	return (x, y)

errorValue = 1e8
def replaceErrors(a):
	return list(map(lambda x: errorValue if x in errors else int(x), a))

def createCombinedAnalysis():
	results = {}

	for tree in trees:
		df_tree = df.\
			filter(items=["heuristic", "ft_basic_events","bdd_size"]) \
			[df.tree == tree]

		h = df_tree["heuristic"].tolist()
		basic_events = replaceErrors(df_tree["ft_basic_events"].tolist())
		bdd_size = replaceErrors(df_tree["bdd_size"].tolist())
		assert(len(h) == len(basic_events) == len(bdd_size))
		
		# We loop over trees so we can sort out trees with similar results for each heuristic
		if sorted(bdd_size)[0] == sorted(bdd_size)[len(bdd_size) - 1]: continue

		# We have to store results so we can add all values for a single heuristic at once
		# This has to happen as it does not recognize "a" and "a" as the same label
		for i in range(len(h)):
			if h[i] in results:
				results[h[i]][0].append(basic_events[i])
				results[h[i]][1].append(bdd_size[i])
			else:
				results[h[i]] = [[basic_events[i]], [bdd_size[i]]]
			
	for heuristic in heuristics:
		if heuristic not in results.keys(): continue
		basic_events = results[heuristic][0]
		bdd_size = results[heuristic][1]
		plt.scatter(basic_events, bdd_size, marker=".", label=heuristic)
	
	saveImage("combined_analysis", xlabel="Basic Events", ylabel="#Nodes", yscale="log", legend=len(results.keys()) > 0)

def createBucketAnalysis():
	# Get minimum cut sets for each Fault Tree
	df_minimal = df
	for error in errors: df_minimal = df_minimal[df.bdd_size != error]
	df_minimal = df_minimal.filter(items=["tree", "bdd_size"]).groupby("tree").min()
	minimumSize = dict(zip(df_minimal.index.tolist(), df_minimal["bdd_size"].tolist()))

	# Create a plot for each heuristic
	for heuristic in heuristics:
		# Entries using the heuristic
		df_heuristic = df[df.heuristic == heuristic]
		# Zip to get a list of (tree, paths) entries
		values = list(zip(df_heuristic["tree"].tolist(), df_heuristic["bdd_size"].tolist()))

		minimumValue = 0.9
		maximumValue = 5
		bucketSize = 0.1

		# Factor function. 0 if we have an error
		factor = lambda x: max(1, int(x[1]) / int(minimumSize[x[0]]))
		factor_filter = lambda x: 0.9 if (x[1] in errors or factor(x) > maximumValue) else factor(x)

		# Map to get list of factor increases with respect to the minimum
		values = list(map(factor_filter, values))

		fileName = "bin_analysis_" + heuristic + ".png"
		print("Bucket Analysis: ", fileName)
		
		bins = int((maximumValue - minimumValue) // bucketSize) 
		_, _, patches = plt.hist(values, bins=bins + 1, range=(minimumValue, maximumValue))
		patches[0].set_color('red')
		saveImage("bin_analysis" + heuristic, 
			xlabel="Factor of nodes compared to minimal solution",
			ylabel="#trees with this factor",
			legend=False)

def createBarPlots():
	x = range(len(heuristics))
	
	for tree in trees:
		values = []
		df_tree = df.filter(items=["tree", "heuristic", "bdd_size"])[df.tree == tree]
		for heuristic in heuristics:
			value = df_tree[df.heuristic == heuristic]["bdd_size"].tolist()
			assert(len(value) == 1)
			values.append(value[0])
		values = replaceErrors(values)

		# Filter out "\" in the string
		fileName = ("bar_plot_" + tree).replace("\\", "_")
		print("Bar plots: ", fileName)
		
		plt.bar(x, values)
		plt.xticks(x, heuristics, rotation=90)
		
		saveImage(fileName, xlabel="Heuristics", ylabel="Size of the BDD", legend=False)

createCombinedAnalysis()
createBucketAnalysis()
createBarPlots()
