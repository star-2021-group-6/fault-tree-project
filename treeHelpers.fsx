#r "nuget: Thoth.Json.Net; GiGraph.Dot"
open GiGraph.Dot.Entities.Attributes.Enums
open GiGraph.Dot.Entities.Graphs
open GiGraph.Dot.Extensions
open Thoth.Json.Net
open System.IO

// Fault tree datastructures
////////////////////////////
type GateType = And | Or | Vote of count: int
type FaultTree =
    | BasicEvent of name: string
    | IntermediateEvent of name: string * gate: GateType * causes: FaultTree list

// Tree Helpers
///////////////
let rec treeMapName f = function
    | BasicEvent name as x -> BasicEvent (f x name)
    | IntermediateEvent (name, gate, causes) as x -> IntermediateEvent (f x name, gate, List.map (treeMapName f) causes)

let getName = function
    | BasicEvent name -> name
    | IntermediateEvent (name, _, _) -> name

module FaultTree =
    let rec eventCount = function
        | BasicEvent _ -> 1
        | IntermediateEvent (_, _, causes) -> 1 + (causes |> List.sumBy eventCount)

    let rec basicEventCount = function
        | BasicEvent _ -> 1
        | IntermediateEvent (_, _, causes) -> causes |> List.sumBy basicEventCount

    let rec maxDepth = function
        | BasicEvent _ -> 0
        | IntermediateEvent (_, _, causes) -> 1 + (causes |> List.map maxDepth |> List.max)

// Nodes and tree conversion
////////////////////////////
[<RequireQualifiedAccess>]
type Node =
    | BasicEvent of name: string
    | IntermediateEvent of name: string * gate: GateType * causes: string list

let getNodeName = function
    | Node.BasicEvent name -> name
    | Node.IntermediateEvent (name, _, _) -> name

let nodeRename f = function
    | Node.BasicEvent name -> Node.BasicEvent (f name)
    | Node.IntermediateEvent (name, gate, causes) -> Node.IntermediateEvent (f name, gate, causes)

let rec treeToNodesList = function
    | BasicEvent name -> 
        [ Node.BasicEvent name ]
    | IntermediateEvent (name, gate, causes) ->
        [ yield! causes |> List.collect treeToNodesList
          yield Node.IntermediateEvent (name, gate, causes |> List.map getName) ]
        |> List.distinct

// JSON generation
//////////////////
let encodeGate = function
    | And -> Encode.object [ "type", Encode.string "and" ]
    | Or -> Encode.object [ "type", Encode.string "or" ]
    | Vote count -> Encode.object [ "type", Encode.string "vote"; "count", Encode.int count ]

let encodeNode = function
    | Node.BasicEvent name ->
        Encode.object [ "name", Encode.string name ]
    | Node.IntermediateEvent (name, gate, causes) ->
        Encode.object [
            "name", Encode.string name
            "gate", encodeGate gate
            "causes", Encode.list (List.map Encode.string causes)
        ]

let nodesListToJson nodesList = Encode.toString 4 (Encode.list (List.map encodeNode nodesList))
let treeToJson = treeToNodesList >> nodesListToJson

let writeTreeToFile filename tree =
    let json = treeToJson tree
    File.WriteAllText(filename, json)

// DOT file generation
//////////////////////
let treeToDot tree =
    let tree = tree |> treeMapName (function 
        | IntermediateEvent (_, Vote count, causes) -> (fun name -> $"{name} ({count}/{causes.Length})") 
        | _ -> id)
    let graph = DotGraph (directed = false)
    for node in treeToNodesList tree do
        match node with
        | Node.BasicEvent name ->
            graph.Nodes.Add(name, fun node -> node.Shape <- DotNodeShape.Circle) |> ignore
        | Node.IntermediateEvent (name, gate, causes) ->
            let shape =
                match gate with
                | And -> DotNodeShape.Trapezium
                | Or -> DotNodeShape.Cylinder
                | Vote _ -> DotNodeShape.Cylinder
            graph.Nodes.Add(name, fun node -> node.Shape <- shape) |> ignore
            for cause in causes do
                graph.Edges.Add(name, cause) |> ignore
    graph

let writeTreeToDotFile filename tree =
    let dot = treeToDot tree
    dot.SaveToFile filename