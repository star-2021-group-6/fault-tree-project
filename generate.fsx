#load "treeHelpers.fsx"
open TreeHelpers
open System.IO

// Fault tree examples
//////////////////////
module ServersAssignment2 =
    let private switch1 = BasicEvent "Switch 1"
    let private switch2 = BasicEvent "Switch 2"
    let private serverSystemFails x =
        IntermediateEvent ($"Server system #{x} fails", Or, [
            BasicEvent $"Server {x}"
            IntermediateEvent ($"Cores connected to system #{x} fail", And, [
                IntermediateEvent ($"Cores #1 connected to system #{x} fail", Or, [
                    switch1
                    BasicEvent $"Cable {x}.1"
                ])
                IntermediateEvent ($"Cores #2 connected to system #{x} fail", Or, [
                    switch2
                    BasicEvent $"Cable {x}.2"
                ])
            ])
        ])

    let basic =
        IntermediateEvent ("System fails", And, [ for x in 1..5 -> serverSystemFails x ])

    let withPurpose =
        IntermediateEvent ("System fails", Or, [
            IntermediateEvent ("Files servers sub-system fails", And, [ for x in 1..2 -> serverSystemFails x ])
            IntermediateEvent ("Email servers sub-system fails", And, [ for x in 4..5 -> serverSystemFails x ])
            IntermediateEvent ("Network server sub-system fails", Or, [
                serverSystemFails 3
                IntermediateEvent ("Network graph disconnected", And, [
                    BasicEvent "Cable 1.2"
                    BasicEvent "Cable 2.2"
                    BasicEvent "Cable 4.1"
                    BasicEvent "Cable 5.1"
                    IntermediateEvent ("One of server 3's cables fails", Or, [
                        BasicEvent "Cable 3.1"
                        BasicEvent "Cable 3.2"
                    ])
                ])
            ])
        ])

let safeRoadTrip =
    let engine = BasicEvent "Engine"
    IntermediateEvent ("Road trip", And, [
        IntermediateEvent ("Phone", Or, [
            BasicEvent "Connection"
            IntermediateEvent ("Power", And, [
                BasicEvent "Battery"
                engine
            ])
        ])
        IntermediateEvent ("Car", Or, [
            engine
            IntermediateEvent ("Tires", Vote 2, [
                for i in 1..4 -> BasicEvent $"Tire {i}"
                yield BasicEvent "Spare"
            ])
        ])
    ])

module FtaOverviewPaper =
    let figure4 =
        IntermediateEvent ("SF", And, [
            IntermediateEvent ("I1", Or, [
                BasicEvent "E1"
                BasicEvent "E2"
            ])
            IntermediateEvent ("I2", Or, [
                BasicEvent "E3"
                BasicEvent "E4"
            ])
        ])

    let figure5 =
        IntermediateEvent ("SF", And, [
            BasicEvent "E1"
            IntermediateEvent ("I1", Or, [
                for i in 2..4 -> BasicEvent $"E{i}"
            ])
        ])

module Nureg =
    // NUREG-0492: Fault Tree Handbook - https://www.nrc.gov/docs/ML1007/ML100780465.pdf

    // Example in ch8, p.109
    let preasureTankRupture =
        IntermediateEvent ("Rupture of pressure tank after the start of pumping", Or, [
            BasicEvent "Tank ruptures due to improper selection of installation (wrong tank)"
            BasicEvent "Tank rupture"
            IntermediateEvent ("Tank rupture (secondary failure)", Or, [
                BasicEvent "Secondary tank failure from other out-of-tolerance conditions"
                IntermediateEvent ("Tank ruptures due to internal over-pressure caused by continuous pump operation for t > 60 sec", Or, [
                    BasicEvent "K2 Relay (secondary failure)"
                    BasicEvent "K2 Relay contacts fail to open"
                    IntermediateEvent ("EMF applied to K2 Relay coil for t>60 sec", And, [
                        IntermediateEvent ("Pressure switch contacts closed for t>60 sec", Or, [
                            BasicEvent "Excess pressure not sensed by pressure actuated switch"
                            BasicEvent "Pressure switch contacts fail to open"
                            BasicEvent "Pressure switch (secondary failure)"
                        ])
                        IntermediateEvent ("EMF remains on pressure switch contacts when pressure switch contacts closed for t>60 sec", Or, [
                            IntermediateEvent ("EMF trhu K1 Relay contacts when pressure switch contacts closed for t>60 sec", Or, [
                                BasicEvent "K1 Relay contacts fail to open"
                                BasicEvent "K1 Relay (secondary failure)"
                                IntermediateEvent ("EMF not removed from K1 Relay coil when pressure switch contacts closed for t>60 sec", Or, [
                                    BasicEvent "Timer does not 'time out' due to improper installation or setting"
                                    BasicEvent "Timer relay contacts fail to open"
                                    BasicEvent "Timer relay (secondary failure)"
                                ])
                            ])
                            IntermediateEvent ("EMF thru S1 switch contacts when pressure switch contacts closed for t>60 sec", Or, [
                                BasicEvent "External reset actuation force remains on switch S1"
                                BasicEvent "S1 switch contacts fail to open"
                                BasicEvent "S1 switch (secondary failure)"
                            ])
                        ])
                    ])
                ])
            ])
        ])

module Random =
    let private weightedRandom (values: (float * 'a) array) (random: System.Random) =
        let cumulative, max = values |> Seq.mapFold (fun cumul (w, _) -> cumul, cumul + w) 0.
        let cumulative = cumulative |> Seq.toArray
        fun () -> 
            let value = random.NextDouble() * max
            let index = System.Array.BinarySearch(cumulative, value)
            let index = if index < 0 then ~~~index - 1 else index
            snd values.[index]

    type [<RequireQualifiedAccess>] private Node = Leaf | And | Or | Vote | ReuseLeaf | ReuseIntermediate
    type private RandomConfig =
        { Leaf: float; And: float; Or: float; Vote: float; ReuseLeaf: float; ReuseIntermediate: float; MaxChildren: int; MaxDepth: int }

    let private random config seed =
        let r = System.Random seed
        let randomNodeType = 
            r |> weightedRandom 
                [| config.Leaf, Node.Leaf
                   config.And, Node.And
                   config.Or, Node.Or
                   config.Vote, Node.Vote
                   config.ReuseLeaf, Node.ReuseLeaf
                   config.ReuseIntermediate, Node.ReuseIntermediate |]
        let rec getRandomNode name depth (allNodes : FaultTree list) =
            let node = if depth < config.MaxDepth then randomNodeType () else Node.Leaf
            let generateChildren count =
                [ 1..count ] 
                |> List.mapFold (fun allNodes i -> 
                    let newNode = getRandomNode $"{name}_{i}" (depth + 1) allNodes
                    (newNode, newNode::allNodes)
                ) allNodes
                |> fst
            match node with
            | Node.Leaf -> 
                BasicEvent name
            | Node.And ->
                let children = r.Next(1, config.MaxChildren + 1)
                IntermediateEvent (name, And, generateChildren children)
            | Node.Or ->
                let children = r.Next(1, config.MaxChildren + 1)
                IntermediateEvent (name, Or, generateChildren children)
            | Node.Vote ->
                let children = r.Next(1, config.MaxChildren + 1)
                let voteCount = r.Next(1, children)
                IntermediateEvent (name, Vote voteCount, generateChildren children)
            | Node.ReuseLeaf ->
                let basics = allNodes |> List.choose (function BasicEvent ev -> Some (BasicEvent ev) | IntermediateEvent _ -> None)
                if basics |> List.isEmpty then
                    BasicEvent name
                else
                    let i = r.Next(0, basics |> List.length)
                    basics.[i]
            | Node.ReuseIntermediate -> 
                let inters = allNodes |> List.choose (function IntermediateEvent (n, g, c) -> Some (IntermediateEvent (n, g, c)) | BasicEvent _ -> None)
                if inters |> List.isEmpty then
                    getRandomNode name depth allNodes
                else
                    let i = r.Next(0, inters |> List.length)
                    inters.[i]
        getRandomNode "0" 0 []

    let private largeConfig = { Leaf = 1.; And = 1.; Or = 1.; Vote = 1.; ReuseLeaf = 0.; ReuseIntermediate = 0.; MaxChildren = 4; MaxDepth = 5 }
    let private deepNarrowConfig = { largeConfig with Leaf = 0.6; MaxChildren = 2; MaxDepth = 10 }
    let private shallowWideConfig = { largeConfig with Leaf = 2.; MaxChildren = 9; MaxDepth = 3 }

    let large = random largeConfig
    let deepNarrow = random deepNarrowConfig
    let shallowWide = random shallowWideConfig
    let largeOnlyAnd = random { largeConfig with And = 3.; Or = 0.; Vote = 0. }
    let largeOnlyOr = random { largeConfig with And = 0.; Or = 3.; Vote = 0. }
    let reusedLeafs = random { largeConfig with ReuseLeaf = 1. }
    let reusedIntermediate = random { largeConfig with ReuseIntermediate = 1. }
    let reused = random { largeConfig with ReuseLeaf = 1.; ReuseIntermediate = 1. }

// Write all trees above to a file
//////////////////////////////////
let randomGens = seq {
    "random/large", Random.large
    "random/deepNarrow", Random.deepNarrow
    "random/shallowWide", Random.shallowWide
    "random/largeOnlyAnd", Random.largeOnlyAnd
    "random/largeOnlyOr", Random.largeOnlyOr
    "random/reusedLeafs", Random.reusedLeafs
    "random/reusedIntermediate", Random.reusedIntermediate
    "random/reused", Random.reused
}

let randomTrees = seq {
    for (name, gen) in randomGens do
        (name, seq { for seed in 1..20 do gen seed })
}

seq {
    "serversAssignment2_basic", ServersAssignment2.basic
    "serversAssignment2_withPurpose", ServersAssignment2.withPurpose
    "safeRoadTrip", safeRoadTrip
    "ftaOverviewPaper_figure4", FtaOverviewPaper.figure4
    "ftaOverviewPaper_figure5", FtaOverviewPaper.figure5
    "nureg_preasureTankRupture", Nureg.preasureTankRupture
    for (name, trees) in randomTrees do
        for (i, tree) in trees |> Seq.indexed do
            ($"{name}_{i}", tree)
} |> Seq.iter (fun (file, tree) ->
    printfn "Writing %s" file
    printfn " with %i events" (FaultTree.eventCount tree)
    printfn "      %i basic events" (FaultTree.basicEventCount tree)
    printfn "      %i maximum depth" (FaultTree.maxDepth tree)
    Directory.CreateDirectory(Path.GetDirectoryName $"./trees/{file}") |> ignore
    writeTreeToFile $"./trees/{file}.json" tree
    writeTreeToDotFile $"./trees/{file}.dot" tree
)

module Stats =
    let summary nums =
        let avg = Seq.average nums
        let min = Seq.min nums
        let max = Seq.max nums
        $"Avg: %5.1f{avg} Min: %5.1f{min} Max: %5.1f{max}"

    let inline summaryBy f seq =
        seq |> Seq.map (f >> float) |> summary

printfn "\nRandom trees summary:"
for (name, trees) in randomTrees do
    printfn "%s" name
    printfn " with %s events" (Stats.summaryBy FaultTree.eventCount trees)
    printfn "      %s basic events" (Stats.summaryBy FaultTree.basicEventCount trees)
    printfn "      %s maximum depth" (Stats.summaryBy FaultTree.maxDepth trees)
