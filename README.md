# Fault Tree examples

Trees are described using the following tree structure:

```fsharp
type GateType = And | Or | Vote of count: int
type FaultTree =
    | BasicEvent of name: string
    | IntermediateEvent of name: string * gate: GateType * causes: FaultTree list
```

The JSON files in the *trees* folder describe a flat list of all nodes, where an intermediate event references the events beneath it in the tree by their names. All nodes of the fault tree appear exactly once in this list. The type of the JSON would look like this in TypeScript:

```typescript
type JSON = Node[]
type Node =
    // Basic event
    | { name: string }
    // Intermediate event
    | { name: string, 
        gate: { type: "and" | "or" } | { type: "vote", count: number },
        causes: string[] }
```

The gate field for an intermediate event has a field `type` containing either "and", "or" or "vote". If the gate is of type "vote", it also has a field `count` for the minimum number of events that should fail to flip this gate. The field `causes` is a list of names of other nodes.
